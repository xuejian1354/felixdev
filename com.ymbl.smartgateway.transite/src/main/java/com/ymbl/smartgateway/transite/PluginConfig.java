package com.ymbl.smartgateway.transite;

public class PluginConfig {
	public static String plugName;
	public static String version;
	public static String plugServer;
	public static String vpnServer;
	public static String gwInfo;
	public static String gwArch;
	public static String gwclib;
	public static String telUser;
	public static String telPass;
	public static int telPort;
	public static int timer;
	public static String macdev;
}
